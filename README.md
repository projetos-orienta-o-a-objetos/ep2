# Snake
Projeto de Orientação a Objetos 2019/02.
O projeto funciona apenas com uma cobra e uma fruta, porem tem implementado as classes das outras cobras e frutas.
# Dependências
1. Instalar a IDE Eclipe
2. Instalar a JVM

# Como compilar

Com o Eclipse aberto faça o seguinte caminho

``` File>Open Projects from File System```

Abrirá uma janela e em Import Source vá em directory abra a pasta do projeto e finalize.

Depois é só clicar no botao Rum do Eclipse


# Como Jogar

1. Inicie clicando em alguma seta.
2. Tente comer o maxmo de as frutas possiveis.
3. Não colida com as barreiras.
