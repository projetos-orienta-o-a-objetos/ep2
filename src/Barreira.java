import java.awt.Image;

import javax.swing.ImageIcon;

public class Barreira {
	private int barreiraX[] = new int[50];
	private int barreiraY[] = new int[50];
	private int quantidadeBarreiras = 50;
	private Image imagemBarreira;
	
	public Barreira() {
		ImageIcon icone = new ImageIcon("img//Barreira.png");
		imagemBarreira = icone.getImage();
		
	}
	public int getBarreiraX(int index) {
		return barreiraX[index];
	}
	public int[] getBarreiraX() {
		return barreiraX;
	}
	public void setBarreiraX(int index, int cordX) {
		barreiraX[index] = cordX;
		
	}
	public int getBarreiraY(int index) {
		return barreiraY[index];
	}
	public int[] getBarreiraY() {
		return barreiraY;
	}
	public void setBarreiraY(int index, int cordY) {
		barreiraY[index] = cordY;
	}
	public Image getImagemBarreira() {
		return imagemBarreira;
	}
	public int getQuantidadeBarreiras() {
		return quantidadeBarreiras;
	}
}
