
public class GameLoop extends Thread {
	private PainelSnake painelJogo; 
	private Evento eventosJogo;
	GameLoop(PainelSnake painelJogo){
		this.painelJogo = painelJogo; 
		 
	}
	public void run () {
		//int cont=0; 
		while (painelJogo.getCobra().isViva()) {
			try {
				Thread.sleep(115);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 
			eventosJogo= new Evento(painelJogo.getCobra(),painelJogo.getFruta(), painelJogo.getBarreiras());
			if(!painelJogo.isPausado()) {
			eventosJogo.VerificaColisoes();
			painelJogo.getCobra().andar();
			}
						painelJogo.repaint();
		}
		painelJogo.repaint();
	}
}
