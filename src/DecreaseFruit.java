import javax.swing.ImageIcon;

public class DecreaseFruit extends SimpleFruit{

	public DecreaseFruit() {
		ImageIcon imagemFruta = new ImageIcon("img//DecreaseFruit.png");
		setFruta(imagemFruta.getImage());
		setTipoFruta(4);
	}
	@Override
	public void poderFruta(CobraComum cobra) {
		cobra.setComprimento(cobra.getComprimentoInicial());
	}
}
