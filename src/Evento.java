import java.util.Random;

public class Evento{
	private CobraComum cobra;
	private Barreira barreiras;
	private SimpleFruit fruta;
	private Random x = new Random();
	private Random y = new Random();
	
	public Evento(CobraComum cobra, SimpleFruit fruta, Barreira barreiras) {
		this.cobra = cobra;
		this.barreiras = barreiras;
		this.fruta = fruta;
		VerificaColisoes();
	}
	
	public void VerificaColisoes() {

			ComeFruta();
			ColisaoBarreiras();
		
		
		
	}
	
	public void ComeFruta() {
		if(cobra.getCoordenadaX(0)== fruta.getXfruta() && cobra.getCoordenadaY(0)==fruta.getYfruta()) {
			adicionaNovaFruta();
			fruta.poderFruta(cobra);
			
		}
	}
	public void adicionaNovaFruta() {
		boolean frutaNaoPosicionada = true;
		while(frutaNaoPosicionada) {
			boolean colidiu = false;
			int cordX = (x.nextInt(31)+2)*20;
			int cordY = ((y.nextInt(26)+5)*20)+10;
			for(int i=0; i<cobra.getComprimento(); i++) {
				if(cordX == cobra.getCoordenadaX(i) && cordY == cobra.getCoordenadaY(i)) {
					colidiu = true;
					break;
				}
			}
			if(!colidiu) {
				fruta.setXfruta(cordX);
				fruta.setYfruta(cordY);
				//System.out.println("FrutaX "+fruta.getXfruta());
				//System.out.println("FrutaY "+fruta.getYfruta());
				frutaNaoPosicionada = false;
			}
		}
	}
	public void ColisaoBarreiras() {
		//System.out.println("Cobra Bateu");
		for(int i = 0; i< barreiras.getQuantidadeBarreiras(); i++){
			if(cobra.getCoordenadaX(0)== barreiras.getBarreiraX(i) && cobra.getCoordenadaY(0)==barreiras.getBarreiraY(i)) {
				cobra.setViva(false);
				
				//System.out.println("Cobra Bateu");
			}
			
		}
		
	}
}
