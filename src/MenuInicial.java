import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JButton;
import javax.swing.JPanel;

public class MenuInicial extends JPanel {
	private JButton botaoCobraComum = new JButton("Cobra Comum");
	private JButton botaoCobraKitty = new JButton("Cobra Kitty");
	private JButton botaocobraStar = new JButton("Cobra Star");

	public MenuInicial() {
		// TODO Auto-generated constructor stub
	}
	
	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(Color.white);
		//g.fillRect(15, 60, 570, 485 );
		g.drawRect(25, 25, 650, 600);
		
		setBackground(Color.gray);
		
		botaoCobraComum.setBounds(40, 200, 150, 50);
		botaoCobraKitty.setBounds(40, 300, 150, 50);
		botaocobraStar.setBounds(40, 400, 150, 50);
		
		add(botaoCobraComum);
		add(botaoCobraKitty);
		add(botaocobraStar);
	}
}
