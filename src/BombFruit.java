import javax.swing.ImageIcon;

public class BombFruit extends SimpleFruit {

	public BombFruit() {
		ImageIcon imagemFruta = new ImageIcon("img//BombFruit.png");
		setFruta(imagemFruta.getImage());
		setTipoFruta(2);
	}
	@Override
	public void poderFruta(CobraComum cobra){
		cobra.setViva(false);
	}
}
