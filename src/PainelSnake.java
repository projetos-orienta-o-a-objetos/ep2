import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

public class PainelSnake extends JPanel{
	
	private CobraComum cobra = new CobraComum();
	private boolean pausado=false;
	private SimpleFruit fruta = new SimpleFruit();
	private Barreira barreiras = new Barreira();
	private Random xBarreira = new Random();
	private Random yBarreira = new Random();
	private Random orientacaoBarreiras = new Random();
	private Random direcaoBarreiras = new Random();
	private boolean inicio = false;
	
	public PainelSnake() {
	
		
	}

	public void paint(Graphics g) {
		super.paint(g);
		
		
		g.setColor(Color.white);
		//g.fillRect(15, 60, 570, 485 );
		g.drawRect(25, 80, 650, 555);
		
		g.setColor(Color.black);
		g.fillRect(26, 81, 649, 554);
		setBackground(Color.gray);
		if(cobra.isViva()) {
			ImageIcon cabeca = new ImageIcon("img//CabecaCobraDireita.png"); 
			ImageIcon corpo = new ImageIcon("img//CorpoCobra.png"); 
			
			Image cabecaimg = cabeca.getImage();
			Image corpoimg = corpo.getImage();
			
			g.drawImage(cabecaimg, cobra.getCoordenadaX(0), cobra.getCoordenadaY(0), this);
			for(int i=1; i<cobra.getComprimento(); i++) {
				g.drawImage(corpoimg, cobra.getCoordenadaX(i), cobra.getCoordenadaY(i), this);
			}
			
			g.drawImage(fruta.getFruta(), fruta.getXfruta(), fruta.getYfruta(), this);
			
			
			if(!inicio) {
				for(int i=0; i< barreiras.getBarreiraX().length ; i++) {
					int cordXBarreira = (xBarreira.nextInt(22)+6)*20; 
					int cordYBarreira = ((yBarreira.nextInt(17)+8)*20)+10;
					barreiras.setBarreiraX(i, cordXBarreira);
					barreiras.setBarreiraY(i, cordYBarreira);
					g.drawImage(barreiras.getImagemBarreira(), barreiras.getBarreiraX(i), barreiras.getBarreiraY(i), this);
					int orientacao =  orientacaoBarreiras.nextInt(2);
					if(orientacao == 0) {
						int direcao = direcaoBarreiras.nextInt(2);
						if(direcao == 0) {
							for(int j = 0; j<4; j++ ) {
								i++;
								barreiras.setBarreiraX(i, barreiras.getBarreiraX(i-1)+20);
								barreiras.setBarreiraY(i, barreiras.getBarreiraY(i-1));
								g.drawImage(barreiras.getImagemBarreira(), barreiras.getBarreiraX(i), barreiras.getBarreiraY(i), this);
								
							}
						}
						else {
							for(int j = 0; j<4; j++ ) {
								i++;
								barreiras.setBarreiraX(i, barreiras.getBarreiraX(i-1)-20);
								barreiras.setBarreiraY(i, barreiras.getBarreiraY(i-1));
								g.drawImage(barreiras.getImagemBarreira(), barreiras.getBarreiraX(i), barreiras.getBarreiraY(i), this);
								
							}
						}
					}else {
						int direcao = direcaoBarreiras.nextInt(2);
						if(direcao == 0) {
							for(int j = 0; j<4; j++ ) {
								i++;
								barreiras.setBarreiraX(i, barreiras.getBarreiraX(i-1));
								barreiras.setBarreiraY(i, barreiras.getBarreiraY(i-1)+20);
								g.drawImage(barreiras.getImagemBarreira(), barreiras.getBarreiraX(i), barreiras.getBarreiraY(i), this);
								
							}
						}
						else {
							for(int j = 0; j<4; j++ ) {
								i++;
								barreiras.setBarreiraX(i, barreiras.getBarreiraX(i-1));
								barreiras.setBarreiraY(i, barreiras.getBarreiraY(i-1)-20);
								g.drawImage(barreiras.getImagemBarreira(), barreiras.getBarreiraX(i), barreiras.getBarreiraY(i), this);
								
							}
							
						}
					}
				}
			}
			else {
				for(int i=0; i< (barreiras.getBarreiraX().length); i++) {
					g.drawImage(barreiras.getImagemBarreira(), barreiras.getBarreiraX(i), barreiras.getBarreiraY(i), this);
				}
			}
			
			inicio = true;
		}
		else {
			//System.out.println("ENtrou no gameover");
			String msg = "GAME OVER";
	        Font fonteGameOver = new Font("Arial", Font.BOLD, 30);
	        Font fonte = new Font("Arial", Font.BOLD, 20);
	        FontMetrics metrica = this.getFontMetrics(fonteGameOver);
	        g.setColor(Color.cyan);
	        g.setFont(fonteGameOver);
	        g.drawString(msg, 200 , 200);
	        
	        msg = "Pontos : "+cobra.getPontos(); 
	        g.setColor(Color.white);
	        g.setFont(fonte);
	        g.drawString(msg,200,260);
	        /*
	        msg = "DESEJA RECOMEÇAR? APERTE ENTER";
	        g.setColor(Color.white);
	        fonte = new Font("Arial", Font.ROMAN_BASELINE, 15);
	        g.setFont(fonte);
	        g.drawString(msg,200,500);*/
	        setPausado(true);

		}
		
	}

	public CobraComum getCobra() {
		return cobra;
	}
	
	public void setCobra(CobraComum cobraComum) {
		this.cobra = cobraComum;
	}

	public boolean isPausado() {
		return pausado;
	}
	
	public void setPausado(boolean pausado) {
		this.pausado = pausado;
	}
	
	public SimpleFruit getFruta() {
		return fruta;
	}
	
	public void setFruta(SimpleFruit fruta) {
		this.fruta = fruta;
	}
	public Barreira getBarreiras() {
		return barreiras;
	}
	
}
