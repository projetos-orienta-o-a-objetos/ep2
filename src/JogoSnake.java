import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;

public class JogoSnake extends JFrame {
	private static PainelSnake painelJogo = new PainelSnake();
	private GameLoop gameLoop = new GameLoop(painelJogo);
	public static void main(String[] args) {
		 
		new JogoSnake();
		
	}
	
	public JogoSnake() {
		setBounds(10, 10, 700, 700);
		//setSize(600, 600);
		setResizable(false);
		setLocationRelativeTo(null);
		setTitle("SnakeGame");
		add(painelJogo);
		gameLoop.start();
		
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent evt) {
				int tecla = evt.getKeyCode();
				if(tecla == KeyEvent.VK_RIGHT && !painelJogo.isPausado()) {
					//System.out.println(tecla);
					painelJogo.getCobra().setDireita(true);
					
				}
				else if(tecla == KeyEvent.VK_LEFT && !painelJogo.isPausado()) {
					//System.out.println(tecla);
					painelJogo.getCobra().setEsquerda(true);
	 
				}
				else if(tecla == KeyEvent.VK_UP && !painelJogo.isPausado()) {
					//System.out.println(tecla);
					painelJogo.getCobra().setCima(true);
					
				}
				else if(tecla == KeyEvent.VK_DOWN && !painelJogo.isPausado()) {
					//System.out.println(tecla);
					painelJogo.getCobra().setBaixo(true);
 
				}
				/*else if(tecla == KeyEvent.VK_ENTER && painelJogo.isPausado()) {
					System.out.println("Entrou no enter");
					painelJogo.setPausado(false);
					painelJogo.getCobra().setViva(true);
					painelJogo = new PainelSnake();
					add(painelJogo);
					
				}*/

			}
		});

		
		
	}
	

}
