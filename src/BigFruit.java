import javax.swing.ImageIcon;

public class BigFruit extends SimpleFruit{
	public BigFruit() {
		ImageIcon imagemFruta = new ImageIcon("img//BigFruit.png");
		setFruta(imagemFruta.getImage());
		setTipoFruta(3);
	}
	
	@Override
	public void poderFruta(CobraComum cobra) {
		cobra.aumentaComprimento(1);
		cobra.setPontos(2);
	}
}
