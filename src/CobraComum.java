
public class CobraComum {
	private boolean viva = true;
	private int tipoCobra ;
	private int comprimentoInicial;
	private int comprimento;
	private int coordenadaX[] = new int [600];
	private int coordenadaY[] = new int [600];
	private int pontos;
	private boolean direita = false;
	private boolean esquerda = false;
	private boolean cima = false;
	private boolean baixo = false;
	
	public CobraComum() {
		setComprimentoInicial(3);
		setComprimento(getComprimentoInicial());
		setTipoCobra(1);
		
		coordenadaX[0] = 100;
		coordenadaX[1] = 80;		
		coordenadaX[2] = 60;
		
		coordenadaY[0] = 90;
		coordenadaY[1] = 90;
		coordenadaY[2] = 90;
	}

	public void andar() {
		if(isDireita()) {
			for(int i = getComprimento()-1; i>0; i--) {
				setCoordenadaX(getCoordenadaX(i-1), i);
				setCoordenadaY(getCoordenadaY(i-1), i);
			}
			setCoordenadaX(getCoordenadaX(0)+20, 0);
			setCoordenadaY(getCoordenadaY(0), 0);
			if(getCoordenadaX(0)>640) {
				setCoordenadaX(40, 0);
			}
		}
		else if(isEsquerda()) {
			for(int i = getComprimento()-1; i>0; i--) {
				setCoordenadaX(getCoordenadaX(i-1), i);
				setCoordenadaY(getCoordenadaY(i-1), i);
			}
			setCoordenadaX(getCoordenadaX(0)-20, 0);
			setCoordenadaY(getCoordenadaY(0), 0);
			if(getCoordenadaX(0)<40) {
				setCoordenadaX(640, 0);
			}
			
		}
		else if(isCima()) {
			for(int i = getComprimento()-1; i>0; i--) {
				setCoordenadaX(getCoordenadaX(i-1), i);
				setCoordenadaY(getCoordenadaY(i-1), i);
			}
			setCoordenadaX(getCoordenadaX(0), 0);
			setCoordenadaY(getCoordenadaY(0)-20, 0);
			if(getCoordenadaY(0)<90) {
				setCoordenadaY(590, 0);
			}
		}
		else if(isBaixo()) {
			for(int i = getComprimento()-1; i>0; i--) {
				setCoordenadaX(getCoordenadaX(i-1), i);
				setCoordenadaY(getCoordenadaY(i-1), i);
			}
			setCoordenadaX(getCoordenadaX(0), 0);
			setCoordenadaY(getCoordenadaY(0)+20, 0);
			if(getCoordenadaY(0)>600) {
				setCoordenadaY(90, 0);
			}
		}
	
		//System.out.println("CX "+getCoordenadaX(0));
		//System.out.println("CY "+getCoordenadaY(0));
	}
	public boolean isViva() {
		return viva;
	}
	
	public void habilidadeEspecial() {
		//A COMUM NAO TEM HABILIDADES ESPECIAIS
	}

	public int getComprimentoInicial() {
		return comprimentoInicial;
	}

	public void setComprimentoInicial(int comprimentoInicial) {
		this.comprimentoInicial = comprimentoInicial;
	}

	public void setViva(boolean viva) {
		this.viva = viva;
	}

	public int getComprimento() {
		return comprimento;
	}

	public void aumentaComprimento(int aumento) {
		comprimento+=aumento;
	}
	public void setComprimento(int comprimento) {
		this.comprimento = comprimento;
	}


	public int getCoordenadaX(int index) {
		return coordenadaX[index];
	}

	public void setCoordenadaX(int coordenada, int index) {
		coordenadaX[index] = coordenada;
	}
	
	public int getCoordenadaY(int index) {
		return coordenadaY[index];
	}

	public void setCoordenadaY(int coordenada, int index) {
		coordenadaY[index] = coordenada;
	}

	public int getPontos() {
		return pontos;
	}

	public void setPontos(int ponto) {
		pontos+=ponto;
	}

	public boolean isDireita() {
		return direita;
	}

	public void setDireita(boolean direita) {
		if(!isEsquerda() && direita) {
			this.direita = direita;
		}
		this.baixo = false;
		this.cima = false;
	}

	public boolean isEsquerda() {
		return esquerda;
	}

	public void setEsquerda(boolean esquerda) {
		if(!isDireita() && esquerda) {
			this.esquerda = esquerda;
		}
		this.baixo = false;
		this.cima = false;
	}

	public boolean isCima() {
		return cima;
	}

	public void setCima(boolean cima) {
		if(!isBaixo() && cima) {
			this.cima = cima;
		}
		this.direita = false;
		this.esquerda = false;
	}

	public boolean isBaixo() {
		return baixo;
	}

	public void setBaixo(boolean baixo) {
		if(!isCima() && baixo) {
			this.baixo = baixo;
		}
		this.direita = false;
		this.esquerda = false;
	}

	public int getTipoCobra() {
		return tipoCobra;
	}

	public void setTipoCobra(int tipoCobra) {
		this.tipoCobra = tipoCobra;
	}
}
