import java.awt.Image;

import javax.swing.ImageIcon;

public class SimpleFruit {
	private int tipoFruta;
	private int xfruta;
	private int yfruta;
	private Image fruta;
	
	public SimpleFruit() {
		setXfruta(340);
		setYfruta(510);
		ImageIcon imagemFruta = new ImageIcon("img//SimpleFruit.png");
		fruta = imagemFruta.getImage();
		tipoFruta =1;		
	}
	public void poderFruta(CobraComum cobra) {
		cobra.aumentaComprimento(1);
		cobra.setPontos(1);
	}
	public int getXfruta() {
		return xfruta;
	}
	
	public void setXfruta(int xfruta) {
		this.xfruta = xfruta;
	}
	
	public int getYfruta() {
		return yfruta;
	}
	
	public void setYfruta(int yfruta) {
		this.yfruta = yfruta;
	}
	
	public Image getFruta() {
		return fruta;
	}
	public int getTipoFruta() {
		return tipoFruta;
	}
	public void setTipoFruta(int tipoFruta) {
		this.tipoFruta = tipoFruta;
	}
	public void setFruta(Image fruta) {
		this.fruta = fruta;
	}

	
}
